# Hamster Blog GraphQL

Backend for [Hamster Blogging Platform](../README.md).

## Getting Started

### Prerequisite

- [Yarn](https://yarnpkg.com/)
- [Node.js](https://nodejs.org/en/download/)
- [Docker](https://www.docker.com/)

Deployment requires [AWS](https://aws.amazon.com/) account.

### Installation

```bash
$ yarn install # Install packages
$ cp .env.dist .env # Copy basic environment variables
$ yarn init:db # Create and load fixtures on local database
```

### Start

```bash
$ yarn start
```

Start with live reload enabled:

```bash
$ yarn start:dev
```

### Test

Note that test is not working at the moment, please test via Frontend or GraphQL Playground.

### Deployment

See: [CI Deployment](../README.md#ci)

## Technical Stack

- [Nest](https://nestjs.com/)
- GraphQL, [schema](./api/schema.graphql) automatically generated using [@nestjs/graphql](https://docs.nestjs.com/graphql/quick-start#code-first)
- Postgres using [typeorm](https://typeorm.io/#/)
- [Cucumber.js](https://github.com/cucumber/cucumber-js) for end to end testing
- Linting using eslint and prettier
- Logging using [winston](https://github.com/winstonjs/winston)
- JWT for Authorization
- Serverless for deployment (and webpack :cry:)

Based on:
- Nest [TypeScript Starter](https://github.com/nestjs/typescript-starter/) repository
- [Serverless GraphQL Guide](https://serverless.com/blog/graphql-api-mysql-postgres-aurora/) for the CloudFormation resources

## Development

To make it easier to add new functionalities to the api, GraphQL schemas and migrations are automatically generated from code. Development can be done entirely on local environment.

1. Start with [live reload](#start)
2. Make changes
3. Run [test](#test)

### Update entities

1. Create or modify entity on `src/models/*.entity.ts`.
2. Runs `yarn typeorm:migration:generate -n migration`
3. Review migration file and run `yarn typeorm:migration:up`
4. Regenerate `schema` by running `yarn start:dev`

### Update resolvers

1. Create or modify resolvers on `src/modules/*.module.ts`
2. Regenerate `schema` by running `yarn start:dev`

## To Do

- [ ] Fix End to end tests. All functionalities are tested using front end's integration tests 👌. GraphQL playground available for manual test
- [ ] Add unit tests. Main functionalities are all related to database operations, need to add this back in when service has more functionalities and logic
- [ ] Integrate Social Sign On, most likely using [Amazon Cognito](https://aws.amazon.com/cognito/)
- [ ] Fix deployed web socket endpoint
- [ ] Docker compose for local development
- [ ] Ensure that service is production ready, see [The Twelve Factors](https://12factor.net/)
- [ ] Reduce deployment size, it's 31.89 MB at the moment 😱
