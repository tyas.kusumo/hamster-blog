import expect from 'expect';
import fetch from 'node-fetch';
import { Given, Then, When } from 'cucumber';
import ApolloClient, { gql } from 'apollo-boost';

// TODO: Api functionalities are tested via ui at the moment
Given('I have an account and I am authenticated', async function () {
  // Set authentication
});

Given('I do not have any existing posts', async function () {
  // Do nothing maybe
});

Given('I have existing posts', async function () {
  // Query GraphQL here
  const client = new ApolloClient({
    fetch: fetch as any,
    uri: 'http://localhost:4000/graphql',
  });

  await client.mutate({
    variables: { title: 'Testing' },
    mutation: gql`
      mutation AddPostMutation($title: String!) {
        addPost(newPostData: { title: $title }) {
          id
        }
      }
    `,
  });
});

When('I retrieve list of my posts', async function () {
  // Query GraphQL here
  const client = new ApolloClient({
    fetch: fetch as any,
    uri: 'http://localhost:4000/graphql',
  });

  this.listResult = await client.query({
    query: gql`
      {
        posts {
          id
          title
        }
      }
    `,
  });
});

Then('I should get an empty list', async function () {
  expect(this.listResult.data.posts).toEqual([]);
});

Then('I should get list of my existing posts', async function () {
  // Check the list
  console.log(this.listResult);
});
