#!/bin/bash
set -e

source .env

SERVER="database";

echo "Stop & remove old docker [$SERVER] and starting new fresh instance of [$SERVER]"
(docker kill $SERVER || :) && \
  (docker rm $SERVER || :) && \
  docker run --name $SERVER -e POSTGRES_PASSWORD=$DATABASE_PASSWORD \
  -e PGPASSWORD=$DATABASE_PASSWORD \
  -p $DATABASE_PORT:$DATABASE_PORT \
  -d postgres

# wait for pg to start
echo "sleep wait for pg-server [$SERVER] to start";
SLEEP 3;

# create the db 
echo "CREATE DATABASE $DATABASE_NAME ENCODING 'UTF-8';" | docker exec -i $SERVER psql -U $DATABASE_USERNAME
echo "\l" | docker exec -i $SERVER psql -U $DATABASE_USERNAME
