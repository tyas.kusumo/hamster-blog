import { writeFileSync } from 'fs';
import AWS from 'aws-sdk';
import { resolve } from 'path';

(async () => {
  const isDeployment = process.env.NODE_ENV === 'production';
  const path = resolve(
    process.cwd(),
    `.env${isDeployment ? '.production' : ''}`,
  );
  require('dotenv').config({ path });

  const config = async () => {
    const baseConfig = {
      type: 'postgres',
      database: process.env.DATABASE_NAME,
      username: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD,
      migrationsTableName: 'migrations',
      entities: ['src/**/*.entity.ts'],
      autoLoadEntities: true,
      migrations: ['src/migrations/*.ts'],
      cli: {
        migrationsDir: 'src/migrations',
      },
      synchronize: true,
    };

    if (!isDeployment) {
      return {
        ...baseConfig,
        host: process.env.DATABASE_HOST,
        port: parseInt(process.env.DATABASE_PORT, 10),
      };
    }

    // Attempt to retrieve host and post from cloud formation stack for deployment
    const credentials = new AWS.SharedIniFileCredentials({
      profile: process.env.ENV,
    });
    AWS.config.credentials = credentials;

    const env = process.env.ENV;
    const cloudformation = new AWS.CloudFormation({
      apiVersion: '2010-05-15',
      region: process.env.DEPLOYMENT_REGION,
    });

    const stackName = `hamster-blog-api-${env}`;
    const stacks = await cloudformation
      .describeStacks({ StackName: stackName })
      .promise()
      .catch(() => ({ Stacks: [] }));

    const stack = stacks.Stacks.find((stk) => {
      return stk.StackName === stackName;
    });

    if (stack && stack.Outputs) {
      const outputs = stack.Outputs.reduce(
        (kv, { OutputKey, OutputValue }) => ({
          ...kv,
          [OutputKey]: OutputValue,
        }),
        {},
      );

      return {
        ...baseConfig,
        host: outputs.PostgreSqlRDSEndpointAddress,
        port: outputs.PostgreSqlRDSEndpointPort,
      };
    }

    return baseConfig;
  };

  writeFileSync('ormconfig.json', JSON.stringify(await config(), null, 2));
})();
