import { resolve } from 'path';

const throwIfNot = <T, K extends keyof T>(
  obj: Partial<T>,
  prop: K,
  msg?: string,
): T[K] => {
  if (obj[prop] === undefined || obj[prop] === null) {
    throw new Error(msg || `Environment is missing variable ${prop}`);
  } else {
    return obj[prop] as T[K];
  }
};

const isDeployment = process.env.NODE_ENV === 'production';
const path = resolve(process.cwd(), `.env${isDeployment ? '.production' : ''}`);
require('dotenv').config({ path });

const requiredEnvs = [
  'DATABASE_NAME',
  'DATABASE_USERNAME',
  'DATABASE_PASSWORD',
  'JWT_SECRET',
  ...(isDeployment
    ? ['ENV', 'DEPLOYMENT_REGION']
    : ['DATABASE_PORT', 'DATABASE_HOST']),
];

requiredEnvs.forEach((v) => {
  throwIfNot(process.env, v);
});
