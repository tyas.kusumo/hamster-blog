#!/bin/bash
set -e

NODE_ENV=production yarn checkenv
NODE_ENV=production yarn run sls deploy
NODE_ENV=production yarn typeorm:migration:up
NODE_ENV=production yarn ts-node scripts/getEndpoints.ts
