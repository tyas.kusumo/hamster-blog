import { writeFileSync } from 'fs';
import AWS from 'aws-sdk';
import { resolve } from 'path';

(async () => {
  const isDeployment = process.env.NODE_ENV === 'production';
  const path = resolve(
    process.cwd(),
    `.env${isDeployment ? '.production' : ''}`,
  );
  require('dotenv').config({ path });

  const config = async (): Promise<{ api: string; websocket: string }> => {
    if (!isDeployment) {
      return {
        api: 'http://localhost:4000/graphql',
        websocket: 'ws://localhost:4000/graphql',
      };
    }

    // Attempt to retrieve host and post from cloud formation stack for deployment
    const credentials = new AWS.SharedIniFileCredentials({
      profile: process.env.ENV,
    });
    AWS.config.credentials = credentials;

    const env = process.env.ENV;
    const cloudformation = new AWS.CloudFormation({
      apiVersion: '2010-05-15',
      region: process.env.DEPLOYMENT_REGION,
    });

    const stackName = `hamster-blog-api-${env}`;
    const stacks = await cloudformation
      .describeStacks({ StackName: stackName })
      .promise()
      .catch(() => ({ Stacks: [] }));

    const stack = stacks.Stacks.find((stk) => {
      return stk.StackName === stackName;
    });

    if (stack && stack.Outputs) {
      const outputs = stack.Outputs.reduce(
        (kv, { OutputKey, OutputValue }) => ({
          ...kv,
          [OutputKey]: OutputValue,
        }),
        {},
      );

      return {
        api: outputs.ServiceEndpoint,
        websocket: outputs.ServiceEndpointWebsocket,
      };
    }
  };

  const endpoints = await config();
  writeFileSync(
    '.endpoints',
    `API_URL=${endpoints.api}\nWEBSOCKET_URL=${endpoints.websocket}`,
  );
})();
