Feature: Posts
  As a social hamster, I want to be able to share posts with hamsters in the network

  Background:
    Given I have an account and I am authenticated

  Scenario: Returns empty list
    Given I do not have any existing posts
    When I retrieve list of my posts
    Then I should get an empty list

  Scenario: Retrieve existing posts
    Given I have existing posts
    When I retrieve list of my posts
    Then I should get list of my existing posts
