/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const StartServerPlugin = require('start-server-webpack-plugin');

module.exports = merge(common, {
  entry: ['webpack/hot/poll?100', './src/index.ts'],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'server.js',
  },
  plugins: [new StartServerPlugin({ name: 'server.js' })],
});
