declare namespace NodeJS {
  // tslint:disable-next-line:interface-name
  export interface ProcessEnv {
    DATABASE_NAME?: string;
    DATABASE_USERNAME?: string;
    DATABASE_PORT?: string;
    DATABASE_HOST?: string;
    JWT_SECRET?: string;
  }
}
