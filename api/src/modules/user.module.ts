/* eslint-disable @typescript-eslint/camelcase */
import {
  Injectable,
  Module,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import {
  Args,
  Field,
  InputType,
  Mutation,
  Query,
  Resolver,
} from '@nestjs/graphql';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import bcrypt from 'bcrypt';
import { MaxLength } from 'class-validator';
import { Repository } from 'typeorm';
import { GqlAuthGuard } from '../auth/gqlAuth.guard';
import { CurrentUser } from '../auth/user.decorator';
import { JWTToken, User } from '../models';

@InputType()
export class LoginUserInput {
  @Field()
  @MaxLength(255)
  username: string;

  @Field()
  @MaxLength(255)
  password: string;
}

@InputType()
export class CreateUserInput {
  @Field()
  @MaxLength(255)
  username: string;

  @Field()
  @MaxLength(255)
  firstName: string;

  @Field()
  @MaxLength(255)
  email: string;

  @Field()
  @MaxLength(255)
  lastName: string;

  @Field()
  password: string;
}

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly repo: Repository<User>,
  ) {}

  async create(data: Partial<User>): Promise<User | undefined> {
    // See: https://github.com/typeorm/typeorm/issues/5530
    const bcryptPassword = await bcrypt.hash(data.password, 10);
    return this.repo.save({ ...data, password: bcryptPassword });
  }

  async validateLogin({
    username,
    password,
  }: LoginUserInput): Promise<Omit<User, 'password'> | null> {
    const user = await this.findOneByUsername(username);

    if (!user) {
      throw new UnauthorizedException();
    }

    const comparePass = await bcrypt.compare(password, user.password);
    if (comparePass) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return result;
    }
    throw new UnauthorizedException();
  }

  async findOneById(id: number): Promise<User> {
    return this.repo.findOne({ where: { id } });
  }

  public async findOneByUsername(username: string): Promise<User | undefined> {
    return this.repo.findOne({ where: { username } });
  }
}

@Resolver(() => User)
export class UserResolver {
  constructor(
    private readonly service: UserService,
    private jwtService: JwtService,
  ) {}

  @Query(() => User)
  @UseGuards(GqlAuthGuard)
  async whoAmI(@CurrentUser() user: User): Promise<User | null> {
    return this.service.findOneByUsername(user.username);
  }

  @Query(() => JWTToken)
  async login(
    @Args('data') data: LoginUserInput,
  ): Promise<JWTToken | undefined> {
    const user = await this.service.validateLogin(data);
    if (user) {
      const access_token = this.jwtService.sign({ username: user.username });
      return { access_token };
    }
    return;
  }

  @Mutation(() => User)
  async createUser(@Args('data') data: CreateUserInput): Promise<User | null> {
    return this.service.create(data);
  }
}

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forFeature([User]),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '60m' }, //TODO: Reduce expiry
    }),
  ],
  providers: [UserService, UserResolver],
  exports: [UserService],
})
export class UserModule {}
