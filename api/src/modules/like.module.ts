import { Injectable, Module, UseGuards } from '@nestjs/common';
import {
  Args,
  Field,
  InputType,
  Mutation,
  Query,
  Resolver,
} from '@nestjs/graphql';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GqlAuthGuard } from '../auth/gqlAuth.guard';
import { CurrentUser } from '../auth/user.decorator';
import { Like, Post, User } from '../models';

@InputType()
export class AddLikeInput {
  @Field()
  postId: string;
}

@InputType()
export class GetLikesInput {
  @Field()
  postId: string;
}

@Injectable()
export class LikeService {
  constructor(
    @InjectRepository(Like) private readonly repo: Repository<Like>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Post) private readonly postRepo: Repository<Post>,
  ) {}

  public async create(postId: string, currentUser: User): Promise<Like> {
    const user = await this.userRepo.findOne({
      where: { username: currentUser.username },
    });
    const post = await this.postRepo.findOne({ where: { id: postId } });

    const like = this.repo.create({ user, post });
    return this.repo.save(like);
  }

  public async findOneByPostId(postId: string): Promise<Like[]> {
    return this.repo
      .createQueryBuilder('like')
      .innerJoinAndSelect('like.post', 'post')
      .innerJoinAndSelect('like.user', 'user')
      .where('post.id = :id', { id: postId })
      .getMany();
  }
}

@Resolver('Likes')
export class LikeResolver {
  constructor(private readonly service: LikeService) {}

  @Mutation(() => Like)
  @UseGuards(GqlAuthGuard)
  async likePost(
    @Args('data') data: AddLikeInput,
    @CurrentUser() user: User,
  ): Promise<Like | null> {
    return this.service.create(data.postId, user);
  }

  @Query(() => [Like])
  @UseGuards(GqlAuthGuard)
  async likesByPost(@Args('data') data: GetLikesInput): Promise<Like[] | null> {
    return this.service.findOneByPostId(data.postId);
  }
}

@Module({
  imports: [
    TypeOrmModule.forFeature([Like]),
    TypeOrmModule.forFeature([Post]),
    TypeOrmModule.forFeature([User]),
  ],
  providers: [LikeService, LikeResolver],
})
export class LikeModule {}
