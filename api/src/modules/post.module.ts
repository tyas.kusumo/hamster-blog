import {
  Injectable,
  Module,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import {
  Args,
  ArgsType,
  Field,
  InputType,
  Int,
  Mutation,
  Query,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { PubSub } from 'apollo-server-express';
import { Max, MaxLength, Min } from 'class-validator';
import { Repository } from 'typeorm';
import { GqlAuthGuard } from '../auth/gqlAuth.guard';
import { CurrentUser } from '../auth/user.decorator';
import { Post, User } from '../models';

@ArgsType()
export class PostArgs {
  @Field(() => Int)
  @Min(0)
  skip = 0;

  @Field(() => Int)
  @Min(1)
  @Max(50)
  take = 25;
}

@InputType()
export class CreatePostInput {
  @Field()
  @MaxLength(300)
  title: string;

  @Field()
  content: string;
}

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post) private readonly repo: Repository<Post>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) {}

  public async create(data: CreatePostInput, currentUser: User): Promise<Post> {
    const user = await this.userRepo.findOne({
      where: { username: currentUser.username },
    });
    const post = this.repo.create({
      ...data,
      user,
    });

    const { id } = await this.repo.save(post);
    return this.findOneById(id);
  }

  public async findOneById(id: string): Promise<Post> {
    return this.repo
      .createQueryBuilder('post')
      .innerJoinAndSelect('post.user', 'user')
      .leftJoinAndSelect('post.likes', 'likes')
      .where('post.id = :id', { id })
      .getOne();
  }

  public async findAll({ skip, take }: PostArgs): Promise<Post[]> {
    return await this.repo
      .createQueryBuilder('post')
      .innerJoinAndSelect('post.user', 'user')
      .leftJoinAndSelect('post.likes', 'likes')
      .addOrderBy('post.createDateTime', 'DESC')
      .skip(skip)
      .take(take)
      .getMany();
  }
}

const pubSub = new PubSub();

@Resolver(() => Post)
export class PostResolver {
  constructor(private readonly service: PostService) {}

  @Query(() => Post)
  @UseGuards(GqlAuthGuard)
  async post(@Args('id') id: string): Promise<Post> {
    const post = await this.service.findOneById(id);
    if (!post) {
      throw new NotFoundException(id);
    }
    return post;
  }

  @Query(() => [Post])
  @UseGuards(GqlAuthGuard)
  async posts(@Args() postArgs: PostArgs): Promise<Post[]> {
    const posts = await this.service.findAll(postArgs);
    return posts;
  }

  @Mutation(() => Post)
  @UseGuards(GqlAuthGuard)
  async createPost(
    @Args('data') data: CreatePostInput,
    @CurrentUser() user: User,
  ): Promise<Post> {
    const post = await this.service.create(data, user);
    pubSub.publish('postAdded', { postAdded: post });
    return post;
  }

  @Subscription(() => Post)
  @UseGuards(GqlAuthGuard)
  postAdded() {
    return pubSub.asyncIterator('postAdded');
  }
}

@Module({
  imports: [TypeOrmModule.forFeature([User]), TypeOrmModule.forFeature([Post])],
  providers: [PostResolver, PostService],
})
export class PostModule {}
