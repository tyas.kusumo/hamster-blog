import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from '../auth/jwt.strategy';

@Module({
  imports: [PassportModule, ConfigModule.forRoot()],
  providers: [JwtStrategy],
  exports: [],
})
export class AuthModule {}
