import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  utilities as nestWinstonModuleUtilities,
  WinstonModule,
} from 'nest-winston';
import winston from 'winston';
import {
  TypeOrmConfigModule,
  TypeOrmConfigService,
} from '../config/database.config';
import { AuthModule } from './auth.module';
import { LikeModule } from './like.module';
import { PostModule } from './post.module';
import { UserModule } from './user.module';

const isDev = process.env.NODE_ENV !== 'production' || process.env.IS_OFFLINE;

@Module({
  imports: [
    WinstonModule.forRoot({
      transports: [
        ...(isDev
          ? [
              new winston.transports.Console({
                level: 'debug',
                format: winston.format.combine(
                  winston.format.timestamp(),
                  nestWinstonModuleUtilities.format.nestLike(),
                ),
              }),
            ]
          : [
              new winston.transports.Console({
                level: 'info',
                format: winston.format.simple(),
              }),
            ]),
      ],
    }),
    GraphQLModule.forRoot({
      installSubscriptionHandlers: true,
      context: ({ req, res }) => ({ req, res }),
      ...(isDev
        ? { autoSchemaFile: 'schema.graphql' }
        : { autoSchemaFile: false }),
      ...(isDev ? {} : { typePaths: ['schema.graphql'] }),
    }),
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [TypeOrmConfigModule],
      useExisting: TypeOrmConfigService,
    }),
    PostModule,
    AuthModule,
    UserModule,
    LikeModule,
  ],
  controllers: [],
})
export class AppModule {}
