import { Logger, Module } from '@nestjs/common';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import path from 'path';
import { ConnectionManager, getConnectionManager } from 'typeorm';

export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  async createTypeOrmOptions(): Promise<TypeOrmModuleOptions> {
    const connectionManager: ConnectionManager = getConnectionManager();

    if (connectionManager.has('default')) {
      await connectionManager.get('default').close();

      Logger.debug('Using default connection...', 'Database');

      return connectionManager.get('default').options;
    } else {
      const options: TypeOrmModuleOptions = {
        type: 'postgres',
        host: process.env.DATABASE_HOST,
        port: parseInt(process.env.DATABASE_PORT, 10),
        database: process.env.DATABASE_NAME,
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        entities: [path.join(__dirname, 'models/**/*.entity{.ts,.js}')],
        autoLoadEntities: true,
        synchronize: false,
      };

      Logger.debug({ options }, 'Database');

      return options;
    }
  }
}

@Module({
  providers: [TypeOrmConfigService],
  exports: [TypeOrmConfigService],
})
export class TypeOrmConfigModule {}
