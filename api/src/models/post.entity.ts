import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Like } from './like.entity';

@Entity('posts')
@ObjectType()
export class Post {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column({ type: 'varchar', length: 300, nullable: false })
  @Field()
  title: string;

  @Column({ type: 'text', nullable: false })
  @Field()
  content: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  @Field()
  createDateTime: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  @Field()
  updatedDateTime: Date;

  @ManyToOne(() => User, (user) => user.posts, { nullable: false })
  @Field(() => User)
  user: User;

  @OneToMany(() => Like, (like) => like.post)
  @Field(() => [Like])
  likes: Post[];
}
