import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  Index,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import { Post } from './post.entity';
import bcrypt from 'bcrypt';

@Entity('users')
@ObjectType()
export class User {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  @Index({ unique: true })
  @Field()
  username: string;

  @Column({ type: 'varchar', length: 255 })
  @Field()
  firstName: string;

  @Column({ type: 'varchar', length: 255 })
  @Field()
  lastName: string;

  @Column({ type: 'varchar', length: 255 })
  @Field()
  @Index({ unique: true })
  email: string;

  @Column({ type: 'varchar', length: 100, nullable: false })
  password: string;

  @BeforeInsert()
  @BeforeUpdate()
  private async bcryptPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  @OneToMany(() => Post, (post) => post.user)
  posts: Post[];
}
