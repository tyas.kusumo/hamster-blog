import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Index,
} from 'typeorm';
import { User } from './user.entity';
import { Post } from './post.entity';

@Entity('likes')
@Index(['post', 'user'], { unique: true }) // User can only like post once
@ObjectType()
export class Like {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  @Field()
  createDateTime: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  @Field()
  updatedDateTime: Date;

  @ManyToOne(() => Post, (post) => post.likes, { nullable: false })
  post: Post;

  @ManyToOne(() => User, (user) => user.posts, { nullable: false })
  user: User;
}
