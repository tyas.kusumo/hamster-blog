import { Field, ID, ObjectType } from '@nestjs/graphql';

// TODO: Return expiry
@ObjectType()
export class JWTToken {
  @Field(() => ID)
  access_token: string;
}
