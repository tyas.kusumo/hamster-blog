/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const slsw = require('serverless-webpack');

module.exports = merge(common, {
  entry: slsw.lib.entries,
  mode: 'production',
});
