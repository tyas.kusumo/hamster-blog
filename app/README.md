# Hamster Blog App

Frontend for [Hamster Blogging Platform](../README.md).

## Introduction

## Getting Started

### Prerequisite
- [Yarn](https://yarnpkg.com/)
- [Node.js](https://nodejs.org/en/download/)

Deployment requires [AWS](https://aws.amazon.com/) account.

### Installation

```bash
$ yarn install # Install packages
$ cp .env.dist .env # Copy basic environment variables
```

### Start

```bash
$ yarn start
```

### Test

Run unit test and snapshot test:

```bash
$ yarn test
```

Run storybook:

```bash
$ yarn storybook
```

Run integration tests:
*Running local api is required to run the integration test.*

```bash
$ yarn cypress:run # Runs in cli
$ yarn cypress:open # Opens the Cypress Test Runner
```

After running the test, screenshots and videos available at:
- `cypress/screenshots`
- `cypress/videos`

## Technical Stack

- React
- MobX State Tree
- Bindings for mobx-state-tree and GraphQL [mst-gql](https://github.com/mobxjs/mst-gql)
- Material UI
- Testing [Cypress](https://www.cypress.io/) and storybook
- Deployed to AWS S3

## Development

To make it easier to add new functionalities to the app, base models are generated from GraphQL schema.

1. Start with [live reload](#start)
2. Make changes
3. Run [test](#test)

### Generate models

Run from main directory

```bash
$ cd ../
$ yarn install # Install main repo
$ yarn generate:model
```

## To Do

- [ ] Create mock server from GraphQL schema
- [ ] Run integration tests using mock server to remove dependencies from backend
- [ ] Show loading animations for better user experience
- [ ] Handle negative response from api
- [ ] Added more negative use cases to the end to end tests
- [ ] Pagination for the list of posts
- [ ] More storybook and snapshot tests for the components
- [ ] Integrate Social Sign On, ensure that it still works for local development
- [ ] Reduce package size
- [ ] Ensure that service is production ready, see [The Twelve Factors](https://12factor.net/)
