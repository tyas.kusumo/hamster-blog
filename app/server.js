var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

const port = process.env.APP_PORT || 3000;

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: false,
  historyApiFallback: true,
  disableHostCheck: true,
}).listen(port, 'localhost', function (err) {
  if (err) {
    console.log(err);
  }

  console.log(`🚀 Server running on http://localhost:${port}`);
});
