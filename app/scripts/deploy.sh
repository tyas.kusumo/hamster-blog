#!/bin/sh
export AWS_BUCKET_NAME=${ENV}-hamster-blog
export AWS_PROFILE=${ENV}

if [ -z "$ENV" ] || [ -z "$DEPLOYMENT_REGION" ] || [ -z "$AWS_PROFILE" ];
  then
    echo "The ENV, DEPLOYMENT_REGION and AWS_PROFILE variables must be set!";
    exit 1;
fi

export UI_ENDPOINT=http://${AWS_BUCKET_NAME}.s3-website-${DEPLOYMENT_REGION}.amazonaws.com

echo "Deploying to $UI_ENDPOINT...";

yarn ts-node scripts/createBucketPermission.ts ${ENV}
if ! aws s3api head-bucket --bucket ${AWS_BUCKET_NAME}
then
  aws s3api create-bucket --bucket ${AWS_BUCKET_NAME} --region "${DEPLOYMENT_REGION}" --create-bucket-configuration LocationConstraint="${DEPLOYMENT_REGION}"
  aws s3 website s3://${AWS_BUCKET_NAME} --index-document index.html --error-document index.html
fi
aws s3api put-bucket-policy --policy file://bucketPolicy.json --bucket ${AWS_BUCKET_NAME} --region "${DEPLOYMENT_REGION}"
aws s3 cp index.html s3://$AWS_BUCKET_NAME/index.html --profile $AWS_PROFILE;
aws s3 sync dist/ s3://$AWS_BUCKET_NAME/dist --acl public-read --profile $AWS_PROFILE;

echo "Deployed to $UI_ENDPOINT";
