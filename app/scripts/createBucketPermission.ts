import { writeFileSync } from 'fs';

writeFileSync(
  'bucketPolicy.json',
  JSON.stringify(
    {
      Version: '2012-10-17',
      Statement: [
        {
          Sid: 'PublicRead',
          Effect: 'Allow',
          Principal: '*',
          Action: ['s3:GetObject'],
          Resource: [`arn:aws:s3:::${process.argv[2]}-hamster-blog/*`],
        },
      ],
    },
    null,
    2,
  ),
);
