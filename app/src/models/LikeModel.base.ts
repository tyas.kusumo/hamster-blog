/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { types } from "mobx-state-tree"
import { QueryBuilder } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { RootStoreType } from "./index"


/**
 * LikeBase
 * auto generated base class for the model LikeModel.
 */
export const LikeModelBase = ModelBase
  .named('Like')
  .props({
    __typename: types.optional(types.literal("Like"), "Like"),
    id: types.identifier,
    createDateTime: types.union(types.undefined, types.frozen()),
    updatedDateTime: types.union(types.undefined, types.frozen()),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  }))

export class LikeModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get createDateTime() { return this.__attr(`createDateTime`) }
  get updatedDateTime() { return this.__attr(`updatedDateTime`) }
}
export function selectFromLike() {
  return new LikeModelSelector()
}

export const likeModelPrimitives = selectFromLike().createDateTime.updatedDateTime
