/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { types } from "mobx-state-tree"
import { MSTGQLRef, QueryBuilder, withTypedRefs } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { LikeModel, LikeModelType } from "./LikeModel"
import { LikeModelSelector } from "./LikeModel.base"
import { UserModel, UserModelType } from "./UserModel"
import { UserModelSelector } from "./UserModel.base"
import { RootStoreType } from "./index"


/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  user: UserModelType;
}

/**
 * PostBase
 * auto generated base class for the model PostModel.
 */
export const PostModelBase = withTypedRefs<Refs>()(ModelBase
  .named('Post')
  .props({
    __typename: types.optional(types.literal("Post"), "Post"),
    id: types.identifier,
    title: types.union(types.undefined, types.string),
    content: types.union(types.undefined, types.string),
    createDateTime: types.union(types.undefined, types.frozen()),
    updatedDateTime: types.union(types.undefined, types.frozen()),
    user: types.union(types.undefined, MSTGQLRef(types.late((): any => UserModel))),
    likes: types.union(types.undefined, types.array(types.late((): any => LikeModel))),
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  })))

export class PostModelSelector extends QueryBuilder {
  get id() { return this.__attr(`id`) }
  get title() { return this.__attr(`title`) }
  get content() { return this.__attr(`content`) }
  get createDateTime() { return this.__attr(`createDateTime`) }
  get updatedDateTime() { return this.__attr(`updatedDateTime`) }
  user(builder?: string | UserModelSelector | ((selector: UserModelSelector) => UserModelSelector)) { return this.__child(`user`, UserModelSelector, builder) }
  likes(builder?: string | LikeModelSelector | ((selector: LikeModelSelector) => LikeModelSelector)) { return this.__child(`likes`, LikeModelSelector, builder) }
}
export function selectFromPost() {
  return new PostModelSelector()
}

export const postModelPrimitives = selectFromPost().title.content.createDateTime.updatedDateTime
