import { Instance } from 'mobx-state-tree';
import { PostModelBase } from './PostModel.base';
import { LikeModelType } from './LikeModel';
import { LIKE_FRAGMENT } from './RootStore';

/* The TypeScript type of an instance of PostModel */
export interface PostModelType extends Instance<typeof PostModel.Type> {}

/* A graphql query fragment builders for PostModel */
export {
  selectFromPost,
  postModelPrimitives,
  PostModelSelector,
} from './PostModel.base';

/**
 * PostModel
 */
export const PostModel = PostModelBase.views((self) => ({
  get likesLength(): number {
    return self.likes?.length || 0;
  },
})).actions((self) => ({
  addLike(like: LikeModelType) {
    const likes = self.likes?.length ? self.likes : [];
    self.likes = [...likes, like];
  },
  like() {
    self.store
      .mutateLikePost({ data: { postId: self.id } }, LIKE_FRAGMENT)
      .then(
        (data) => {
          if (data.likePost) {
            this.addLike(data.likePost);
          }
        },
        () => {
          // TODO: Check the type of error, ignore for now since it's most likely double up on likes
        },
      );
  },
}));
