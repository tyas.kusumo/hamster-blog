import { Instance } from 'mobx-state-tree';
import { UserModelBase, selectFromUser } from './UserModel.base';

/* The TypeScript type of an instance of UserModel */
export interface UserModelType extends Instance<typeof UserModel.Type> {}

/* A graphql query fragment builders for UserModel */
export {
  selectFromUser,
  userModelPrimitives,
  UserModelSelector,
} from './UserModel.base';

export const USER_FRAGMENT = selectFromUser().id.username.email.firstName.lastName.toString();

/**
 * UserModel
 */
export const UserModel = UserModelBase.props({}).actions(() => ({}));
