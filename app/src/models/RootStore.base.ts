/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */
import { ObservableMap } from "mobx"
import { types } from "mobx-state-tree"
import { MSTGQLStore, configureStoreMixin, QueryOptions, withTypedRefs } from "mst-gql"

import { LikeModel, LikeModelType } from "./LikeModel"
import { likeModelPrimitives, LikeModelSelector } from "./LikeModel.base"
import { PostModel, PostModelType } from "./PostModel"
import { postModelPrimitives, PostModelSelector } from "./PostModel.base"
import { UserModel, UserModelType } from "./UserModel"
import { userModelPrimitives, UserModelSelector } from "./UserModel.base"
import { JWTTokenModel, JWTTokenModelType } from "./JWTTokenModel"
import { jWTTokenModelPrimitives, JWTTokenModelSelector } from "./JWTTokenModel.base"


export type GetLikesInput = {
  postId: string
}
export type LoginUserInput = {
  username: string
  password: string
}
export type AddLikeInput = {
  postId: string
}
export type CreatePostInput = {
  title: string
  content: string
}
export type CreateUserInput = {
  username: string
  firstName: string
  email: string
  lastName: string
  password: string
}
/* The TypeScript type that explicits the refs to other models in order to prevent a circular refs issue */
type Refs = {
  posts: ObservableMap<string, PostModelType>,
  users: ObservableMap<string, UserModelType>
}

/**
* Store, managing, among others, all the objects received through graphQL
*/
export const RootStoreBase = withTypedRefs<Refs>()(MSTGQLStore
  .named("RootStore")
  .extend(configureStoreMixin([['Like', () => LikeModel], ['Post', () => PostModel], ['User', () => UserModel], ['JWTToken', () => JWTTokenModel]], ['Post', 'User']))
  .props({
    posts: types.optional(types.map(types.late((): any => PostModel)), {}),
    users: types.optional(types.map(types.late((): any => UserModel)), {})
  })
  .actions(self => ({
    queryLikesByPost(variables: { data: GetLikesInput }, resultSelector: string | ((qb: LikeModelSelector) => LikeModelSelector) = likeModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ likesByPost: LikeModelType[]}>(`query likesByPost($data: GetLikesInput!) { likesByPost(data: $data) {
        ${typeof resultSelector === "function" ? resultSelector(new LikeModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryPost(variables: { id: string }, resultSelector: string | ((qb: PostModelSelector) => PostModelSelector) = postModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ post: PostModelType}>(`query post($id: String!) { post(id: $id) {
        ${typeof resultSelector === "function" ? resultSelector(new PostModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryPosts(variables: { skip?: number, take?: number }, resultSelector: string | ((qb: PostModelSelector) => PostModelSelector) = postModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ posts: PostModelType[]}>(`query posts($skip: Int, $take: Int) { posts(skip: $skip, take: $take) {
        ${typeof resultSelector === "function" ? resultSelector(new PostModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryWhoAmI(variables?: {  }, resultSelector: string | ((qb: UserModelSelector) => UserModelSelector) = userModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ whoAmI: UserModelType}>(`query whoAmI { whoAmI {
        ${typeof resultSelector === "function" ? resultSelector(new UserModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    queryLogin(variables: { data: LoginUserInput }, resultSelector: string | ((qb: JWTTokenModelSelector) => JWTTokenModelSelector) = jWTTokenModelPrimitives.toString(), options: QueryOptions = {}) {
      return self.query<{ login: JWTTokenModelType}>(`query login($data: LoginUserInput!) { login(data: $data) {
        ${typeof resultSelector === "function" ? resultSelector(new JWTTokenModelSelector()).toString() : resultSelector}
      } }`, variables, options)
    },
    mutateLikePost(variables: { data: AddLikeInput }, resultSelector: string | ((qb: LikeModelSelector) => LikeModelSelector) = likeModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ likePost: LikeModelType}>(`mutation likePost($data: AddLikeInput!) { likePost(data: $data) {
        ${typeof resultSelector === "function" ? resultSelector(new LikeModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
    mutateCreatePost(variables: { data: CreatePostInput }, resultSelector: string | ((qb: PostModelSelector) => PostModelSelector) = postModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ createPost: PostModelType}>(`mutation createPost($data: CreatePostInput!) { createPost(data: $data) {
        ${typeof resultSelector === "function" ? resultSelector(new PostModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
    mutateCreateUser(variables: { data: CreateUserInput }, resultSelector: string | ((qb: UserModelSelector) => UserModelSelector) = userModelPrimitives.toString(), optimisticUpdate?: () => void) {
      return self.mutate<{ createUser: UserModelType}>(`mutation createUser($data: CreateUserInput!) { createUser(data: $data) {
        ${typeof resultSelector === "function" ? resultSelector(new UserModelSelector()).toString() : resultSelector}
      } }`, variables, optimisticUpdate)
    },
    subscribePostAdded(variables?: {  }, resultSelector: string | ((qb: PostModelSelector) => PostModelSelector) = postModelPrimitives.toString(), onData?: (item: any) => void) {
      return self.subscribe<{ postAdded: PostModelType}>(`subscription postAdded { postAdded {
        ${typeof resultSelector === "function" ? resultSelector(new PostModelSelector()).toString() : resultSelector}
      } }`, variables, onData)
    },
  })))
