import { Instance } from 'mobx-state-tree';
import { LikeModelBase } from './LikeModel.base';

/* The TypeScript type of an instance of LikeModel */
export interface LikeModelType extends Instance<typeof LikeModel.Type> {}

/* A graphql query fragment builders for LikeModel */
export {
  selectFromLike,
  likeModelPrimitives,
  LikeModelSelector,
} from './LikeModel.base';

/**
 * LikeModel
 */
export const LikeModel = LikeModelBase.actions(() => ({}));
