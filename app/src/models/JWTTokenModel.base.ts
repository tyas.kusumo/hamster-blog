/* This is a mst-gql generated file, don't modify it manually */
/* eslint-disable */
/* tslint:disable */

import { types } from "mobx-state-tree"
import { QueryBuilder } from "mst-gql"
import { ModelBase } from "./ModelBase"
import { RootStoreType } from "./index"


/**
 * JWTTokenBase
 * auto generated base class for the model JWTTokenModel.
 */
export const JWTTokenModelBase = ModelBase
  .named('JWTToken')
  .props({
    __typename: types.optional(types.literal("JWTToken"), "JWTToken"),
    access_token: types.identifier,
  })
  .views(self => ({
    get store() {
      return self.__getStore<RootStoreType>()
    }
  }))

export class JWTTokenModelSelector extends QueryBuilder {
  get access_token() { return this.__attr(`access_token`) }
}
export function selectFromJWTToken() {
  return new JWTTokenModelSelector()
}

export const jWTTokenModelPrimitives = selectFromJWTToken().access_token
