import { History } from 'history';
import { getEnv, Instance, types } from 'mobx-state-tree';
import moment from 'moment';
import { selectFromLike } from './LikeModel';
import { PostModel, selectFromPost } from './PostModel';
import {
  RootStoreBase,
  CreatePostInput,
  CreateUserInput,
  LoginUserInput,
} from './RootStore.base';
import { selectFromJWTToken } from './JWTTokenModel.base';

export interface RootStoreType extends Instance<typeof RootStore.Type> {}

export const POST_FRAGMENT = selectFromPost()
  .id.title.createDateTime.updatedDateTime.content.user((user) => user.username)
  .likes((like) => like.id.createDateTime.updatedDateTime)
  .toString();
export const LIKE_FRAGMENT = selectFromLike().id.createDateTime.updatedDateTime.toString();
export const JWT_FRAGMENT = selectFromJWTToken().access_token.toString();

// TODO: Move login flow out of root store, also handle JWT expiry
export const RootStore = RootStoreBase.props({
  loggedIn: types.optional(types.boolean, false),
  postList: types.optional(types.map(types.reference(PostModel)), {}),
  alert: types.model({
    severity: types.optional(types.string, ''),
    message: types.optional(types.string, ''),
  }),
})
  .views((self) => ({
    get postSortedByNew() {
      const sortedPosts = new Map(
        Array.from(self.posts).sort(([, a], [, b]) => {
          return (
            moment(b.createDateTime).unix() - moment(a.createDateTime).unix()
          );
        }),
      );
      return sortedPosts;
    },
  }))
  .actions((self) => ({
    afterCreate() {
      // Check if there is stored JWT in local storage to see if user is logged in
      const jwt = window.localStorage.getItem('jwt');
      if (jwt) {
        self.loggedIn = true;
        getEnv(self).gqlHttpClient.setHeaders({
          authorization: `bearer ${jwt}`,
        });
      }
      return;
    },
    setLoggedIn(loggedIn: boolean) {
      self.loggedIn = loggedIn;
    },
    setAlert({
      message = '',
      severity = '',
    }: {
      message?: string;
      severity?: string;
    }) {
      self.alert = { message, severity };
    },
    async login(data: LoginUserInput, history: History) {
      this.setAlert({});

      self.queryLogin({ data }, JWT_FRAGMENT).then(
        (result) => {
          const jwt = result.login.access_token;

          if (jwt) {
            window.localStorage.setItem('jwt', jwt);
            getEnv(self).gqlHttpClient.setHeaders({
              authorization: `bearer ${jwt}`,
            });
            this.setLoggedIn(true);
            history.push('/');
          } else {
            this.setAlert({ severity: 'error', message: 'Login Failed!' });
          }
        },
        () => {
          this.setAlert({ severity: 'error', message: 'Login Failed!' });
        },
      );
    },
    logout() {
      window.localStorage.removeItem('jwt');
      return;
    },
    createPost(data: CreatePostInput) {
      // TODO: Error handling for creating post
      self.mutateCreatePost({ data }, POST_FRAGMENT);
    },
    createUser(data: CreateUserInput, history: History) {
      this.setAlert({});

      self.mutateCreateUser({ data }).then(
        () => {
          this.setAlert({
            severity: 'success',
            message: 'Registration successful, please log in!',
          });
          history.push('/');
        },
        () => {
          this.setAlert({ severity: 'error', message: 'Registration failed!' });
        },
      );

      this.setAlert({ severity: 'error', message: 'Registration failed!' });
    },
  }));
