import { Instance } from 'mobx-state-tree';
import { JWTTokenModelBase } from './JWTTokenModel.base';

/* The TypeScript type of an instance of JWTTokenModel */
export interface JWTTokenModelType
  extends Instance<typeof JWTTokenModel.Type> {}

/* A graphql query fragment builders for JWTTokenModel */
export {
  selectFromJWTToken,
  jWTTokenModelPrimitives,
  JWTTokenModelSelector,
} from './JWTTokenModel.base';

/**
 * JWTTokenModel
 */
export const JWTTokenModel = JWTTokenModelBase.actions(() => ({}));
