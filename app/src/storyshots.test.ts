import initStoryshots from '@storybook/addon-storyshots';
import { resolve } from 'path';

initStoryshots({ configPath: resolve(__dirname, '../.storybook') });
