import React from 'react';
import { Alert } from '../Alert';

export default {
  component: Alert,
  title: 'Alert',
};

export const Error = () => <Alert severity="error">Login failed!</Alert>;

export const Success = () => (
  <Alert severity="success">Registration successful, please log in!</Alert>
);
