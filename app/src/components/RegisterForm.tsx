import React from 'react';
import {
  Avatar,
  Button,
  Container,
  CssBaseline,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { LockOutlined as LockOutlinedIcon } from '@material-ui/icons';
import { History } from 'history';
import { observer } from 'mobx-react-lite';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { Alert } from '../components/Alert';
import { RootStoreType } from '../models';

export const RegisterForm = observer(
  ({ store, history }: { store: RootStoreType; history: History }) => {
    const classes = useStyles();

    const [values, setValues] = React.useState({
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      username: '',
    });

    const handleInputChange = (e: any) => {
      const { name, value } = e.target;
      setValues({ ...values, [name]: value });
    };

    const register = async () => {
      store.createUser(values, history);
    };

    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          {store.alert.severity && store.alert.message && (
            <Alert severity={store.alert.severity as any}>
              {store.alert.message}
            </Alert>
          )}
          <ValidatorForm
            className={classes.form}
            noValidate
            onSubmit={register}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextValidator
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  value={values.firstName}
                  onChange={handleInputChange}
                  validators={[
                    'required',
                    'minStringLength:0',
                    'maxStringLength:255',
                  ]}
                  errorMessages={[
                    'this field is required',
                    'between 0 and 255 characters',
                    'between 0 and 255 characters',
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextValidator
                  variant="outlined"
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                  onChange={handleInputChange}
                  value={values.lastName}
                  validators={[
                    'required',
                    'minStringLength:0',
                    'maxStringLength:255',
                  ]}
                  errorMessages={[
                    'this field is required',
                    'between 0 and 255 characters',
                    'between 0 and 255 characters',
                  ]}
                />
              </Grid>
              <Grid item xs={12}>
                <TextValidator
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  onChange={handleInputChange}
                  value={values.email}
                  validators={['required', 'isEmail']}
                  errorMessages={[
                    'this field is required',
                    'is not a valid email address',
                  ]}
                />
              </Grid>
              <Grid item xs={12}>
                <TextValidator
                  variant="outlined"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoComplete="username"
                  onChange={handleInputChange}
                  value={values.username}
                  validators={[
                    'required',
                    'minStringLength:0',
                    'maxStringLength:255',
                  ]}
                  errorMessages={[
                    'this field is required',
                    'between 0 and 255 characters',
                    'between 0 and 255 characters',
                  ]}
                />
              </Grid>
              <Grid item xs={12}>
                <TextValidator
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={handleInputChange}
                  value={values.password}
                  validators={[
                    'required',
                    'minStringLength:0',
                    'maxStringLength:255',
                  ]}
                  errorMessages={[
                    'this field is required',
                    'between 0 and 255 characters',
                    'between 0 and 255 characters',
                  ]}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
          </ValidatorForm>
        </div>
      </Container>
    );
  },
);

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
