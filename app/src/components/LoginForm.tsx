import React from 'react';
import {
  Avatar,
  Button,
  Container,
  CssBaseline,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { LockOutlined as LockOutlinedIcon } from '@material-ui/icons';
import { History } from 'history';
import { observer } from 'mobx-react-lite';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { Alert } from '../components/Alert';
import { RootStoreType } from '../models';

export const LoginForm = observer(
  ({ store, history }: { store: RootStoreType; history: History }) => {
    const classes = useStyles();

    const [values, setValues] = React.useState({
      username: '',
      password: '',
    });

    const handleInputChange = (e: any) => {
      const { name, value } = e.target;
      setValues({ ...values, [name]: value });
    };

    const login = async () => {
      await store.login(values, history);
    };

    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          {store.alert.severity && store.alert.message && (
            <Alert severity={store.alert.severity as any}>
              {store.alert.message}
            </Alert>
          )}
          <ValidatorForm className={classes.form} noValidate onSubmit={login}>
            <TextValidator
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              value={values.username}
              onChange={handleInputChange}
              autoFocus
              validators={['required']}
              errorMessages={['this field is required']}
            />
            <TextValidator
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={values.password}
              onChange={handleInputChange}
              validators={['required']}
              errorMessages={['this field is required']}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
          </ValidatorForm>
        </div>
      </Container>
    );
  },
);

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
