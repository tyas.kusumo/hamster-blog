import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { useQuery } from '../models';

export const PrivateRoute = observer(
  ({ component: Component, ...rest }: any) => {
    const { store } = useQuery();
    return (
      <Route
        {...rest}
        render={(props) =>
          store.loggedIn ? <Component {...props} /> : <Redirect to="/login" />
        }
      />
    );
  },
);
