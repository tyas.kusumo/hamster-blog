import React from 'react';
import { makeStyles } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

export const Alert = (props: AlertProps) => {
  const classes = useStyles();
  return (
    <MuiAlert
      elevation={6}
      variant="filled"
      className={classes.fill}
      {...props}
    />
  );
};

const useStyles = makeStyles(() => ({
  fill: {
    width: '100%',
  },
}));
