import React from 'react';
import { Link, Typography, TypographyProps } from '@material-ui/core';
import { Copyright as CopyrightIcon } from '@material-ui/icons';

export const Copyright = (props: TypographyProps) => (
  <Typography
    variant="body2"
    align="center"
    color="textSecondary"
    style={{ paddingTop: 10 }}
    {...props}
  >
    Copyright <CopyrightIcon style={{ fontSize: 11 }} />{' '}
    <Link color="inherit" href="#">
      Hamster Blog
    </Link>{' '}
    {new Date().getFullYear()}
  </Typography>
);
