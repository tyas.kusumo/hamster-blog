import React from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  makeStyles,
} from '@material-ui/core';
import { Pets as PetsIcon } from '@material-ui/icons';
import { RootStoreType } from '../models';
import { History } from 'history';

export const Navigation = ({
  store,
  history,
}: {
  store: RootStoreType;
  history: History;
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography
            component="h1"
            variant="h4"
            className={classes.title}
            id="page-title"
          >
            <PetsIcon
              fontSize="large"
              color="action"
              style={{ paddingRight: 10 }}
            />
            Hamster Blog
          </Typography>
          <form>
            <Button
              type="submit"
              color="inherit"
              onClick={async () => {
                await store.logout();
                history.push('/');
              }}
            >
              Logout
            </Button>
          </form>
        </Toolbar>
      </AppBar>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    paddingLeft: 50,
    flexGrow: 1,
    fontSize: 40,
  },
}));
