import React from 'react';
import {
  Card,
  CardActions,
  CardActionsProps,
  CardContent,
  CardHeader,
  Container,
  IconButton,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { Favorite as FavoriteIcon } from '@material-ui/icons';
import { observer } from 'mobx-react-lite';
import moment from 'moment';
import { PostModelType, RootStoreType } from '../models';

interface ILikesProps extends CardActionsProps {
  post: PostModelType;
}

export const Likes = observer((props: ILikesProps) => {
  const { post, ...cardActionProps } = props;

  const hasLikes = post.likesLength > 0;
  const getLikesText = (count: number) => {
    return count > 0
      ? count + ' Like' + (count > 1 ? 's' : '')
      : 'No likes yet :(';
  };

  return (
    <CardActions {...cardActionProps}>
      <Tooltip title="Like">
        <IconButton
          size="small"
          onClick={() => {
            post.like();
          }}
        >
          <FavoriteIcon color={hasLikes ? 'secondary' : 'disabled'} />
        </IconButton>
      </Tooltip>
      <Typography
        variant="body2"
        color={hasLikes ? 'secondary' : 'textSecondary'}
      >
        {getLikesText(post.likesLength)}
      </Typography>
    </CardActions>
  );
});

const Post = ({ post, row }: { post: PostModelType; row: number }) => {
  const classes = useStyles();
  return (
    <Card className={classes.card} id={`posts-card-${row}`}>
      <CardHeader id={`posts-title-${row}`} title={post.title} />
      <CardContent>
        <Typography variant="body2" display="block" gutterBottom>
          {moment(post.updatedDateTime).format('LLL')}
        </Typography>
        <Typography paragraph id={`posts-content-${row}`}>
          {post.content}
        </Typography>
        <Typography variant="overline" display="block" gutterBottom>
          {'Posted by: ' + post.user.username}
        </Typography>
      </CardContent>
      <Likes post={post} id={`posts-action-${row}`} />
    </Card>
  );
};

export const Posts = observer(({ store }: { store: RootStoreType }) => {
  return (
    <Container id="post-list">
      {Array.from(store.postSortedByNew.values()).map((post, row) => (
        <Post key={post.id} post={post} row={row} />
      ))}
    </Container>
  );
});

const useStyles = makeStyles((theme) => ({
  card: {
    margin: theme.spacing(2),
  },
}));
