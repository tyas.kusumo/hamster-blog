import React from 'react';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  createStyles,
  Fab,
  Grid,
  makeStyles,
  Theme,
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { RootStoreType } from '../models';

const CreatePostForm = ({
  parentState,
  store,
}: {
  parentState: any;
  store: RootStoreType;
}) => {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    title: '',
    content: '',
  });

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const save = () => {
    store.createPost(values);
    setValues({ title: '', content: '' });
    cancel();
  };

  const cancel = () =>
    parentState.setValues({ ...parentState.values, show: false });

  return (
    <Card className={classes.card} variant="elevation">
      <CardHeader title="Create Post"></CardHeader>
      <CardContent>
        <ValidatorForm className={classes.form} noValidate onSubmit={save}>
          <Grid container spacing={2} style={{ padding: 20 }}>
            <Grid item xs={6}>
              <TextValidator
                id="title"
                name="title"
                label="Title"
                variant="outlined"
                required
                fullWidth
                autoFocus
                onChange={handleInputChange}
                value={values.title}
                validators={['required']}
                errorMessages={['this field is required']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextValidator
                id="content"
                name="content"
                label="Content"
                onChange={handleInputChange}
                value={values.content}
                variant="outlined"
                required
                multiline
                rows={10}
                fullWidth={true}
                validators={['required']}
                errorMessages={['this field is required']}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="default"
                className={classes.button}
                onClick={cancel}
              >
                Cancel
              </Button>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </ValidatorForm>
      </CardContent>
    </Card>
  );
};

export const CreatePost = ({ store }: { store: RootStoreType }) => {
  const classes = useStyles();
  const [values, setValues] = React.useState({ show: false });

  const toggle = () => setValues({ ...values, show: !values.show });

  return (
    <Container>
      <Fab
        color="secondary"
        aria-label="add"
        onClick={toggle}
        className={classes.addButton}
        id="create-post"
      >
        <AddIcon />
      </Fab>
      {values.show && (
        <CreatePostForm parentState={{ values, setValues }} store={store} />
      )}{' '}
    </Container>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      margin: theme.spacing(2),
    },
    addButton: {
      position: 'absolute',
      zIndex: 1,
      top: 36,
      left: 10,
    },
    removeButton: {
      position: 'absolute',
      zIndex: 1,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
    },
    button: {
      marginRight: theme.spacing(2),
    },
  }),
);
