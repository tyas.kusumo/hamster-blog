import React from 'react';

import { createHttpClient } from 'mst-gql';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { StoreContext } from './models/reactUtils';
import { RootStore } from './models/RootStore';
import { Login } from './pages/Login';
import { PrivateRoute } from './components/PrivateRoute';
import { Home } from './pages/Home';
import { Register } from './pages/Register';
import { SubscriptionClient } from 'subscriptions-transport-ws';

import 'typeface-roboto';

const gqlHttpClient = createHttpClient(`${process.env.API_URL}/graphql`);
const gqlWsClient = new SubscriptionClient(
  `${process.env.WEBSOCKET_URL}/graphql`,
  {
    reconnect: false,
  },
);

const rootStore = RootStore.create(
  { alert: {} },
  {
    gqlHttpClient,
    gqlWsClient,
  },
);

ReactDOM.render(
  <StoreContext.Provider value={rootStore}>
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <PrivateRoute path="/" component={Home} />
      </Switch>
    </Router>
  </StoreContext.Provider>,
  document.getElementById('root'),
);
