import { Box, Container, Link, Typography } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { Copyright } from '../components/Copyright';
import { useQuery } from '../models';
import { RegisterForm } from '../components/RegisterForm';

export const Register = observer(() => {
  const { store } = useQuery();
  const history = useHistory();

  return (
    <>
      <Container component="main" maxWidth="xs">
        <RegisterForm store={store} history={history} />
        <Typography variant="body2" color="textSecondary" align="center">
          <Link
            to="/login"
            variant="body2"
            align="center"
            component={RouterLink}
          >
            Already have an account? Sign in
          </Link>
        </Typography>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
    </>
  );
});
