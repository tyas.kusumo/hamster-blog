import React from 'react';
import { CreatePost } from '../components/CreatePost';
import { Navigation } from '../components/Navigation';
import { Copyright } from '../components/Copyright';
import { Grid } from '@material-ui/core';
import { Posts } from '../components/Posts';
import { observer } from 'mobx-react-lite';
import { useQuery, POST_FRAGMENT } from '../models';
import { useHistory } from 'react-router-dom';

export const Home = observer(() => {
  const { store } = useQuery((store) =>
    store.queryPosts({ skip: 0, take: 20 }, POST_FRAGMENT),
  );
  const history = useHistory();

  return (
    <Grid>
      <Navigation store={store} history={history} />
      <CreatePost store={store} />
      <Posts store={store} />
      <Copyright style={{ color: '#fff', paddingBottom: 20 }} />
    </Grid>
  );
});
