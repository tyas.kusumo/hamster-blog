import React from 'react';

import { Box, Container, Link, Typography } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { Copyright } from '../components/Copyright';
import { LoginForm } from '../components/LoginForm';
import { useQuery } from '../models';

export const Login = observer(() => {
  const { store } = useQuery();
  const history = useHistory();

  return (
    <>
      <Container component="main" maxWidth="xs">
        <LoginForm store={store} history={history} />
        <Typography variant="body2" color="textSecondary" align="center">
          <Link
            to="/register"
            variant="body2"
            align="center"
            component={RouterLink}
          >
            {"Don't have an account? Sign Up"}
          </Link>
        </Typography>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
    </>
  );
});
