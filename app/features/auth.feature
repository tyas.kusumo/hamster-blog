Feature: Auth
  As a hamster
  I want to sign up to a platform 
  So I can connect with the other hamsters on the network

  @validation
  Scenario: Show error message when login with invalid details
    Given I am on login page
    Then I see a login form
    When I fill "testing" for "username"
    And I fill "lalala" for "password"
    And I click "Sign In" button
    Then I see "Login Failed!" message  
  
  @authentication
  Scenario: Show login page if not logged in
    Given I am on the home page and I am not logged in
    Then I see a login form

  #TODO: Separate these scenarios
  @signup @login @logout
  Scenario: Sign up for new account with valid details
    Given I am on login page
    When I click "Don't have an account? Sign Up" link
    # Register for a new account
    Then I see a registration form
    When I fill "{{name.firstName}}" for "firstName"
    And I fill "{{name.lastName}}" for "lastName"
    And I fill "{{internet.email}}" for "email"
    And I fill "{{internet.userName}}" for "username"
    And I fill "testing" for "password"
    And I click "Sign Up" button
    Then I see "Registration successful, please log in!" message
    # Login
    And I see a login form
    And I fill "@username" for "username"
    And I fill "@password" for "password"
    And I click "Sign In" button
    Then I successfully logged in
    # Logout
    And I click "Logout" button
    Then I successfully logged out
