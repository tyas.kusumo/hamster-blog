Feature: Posts
  As a social hamster I want to be able to share posts with hamsters in the network
  As a social hamster I want the hamsters in the network to like my posts

  #TODO: separate these scenarios
  Scenario: Share new post, list post and like post
    Given I am logged in
    # Create new post
    And I click on create post button
    Then I should see create post form
    # Test to ensure that cancel button works
    When I click "Cancel" button
    Then I should not see create post form
    # Create new post and see if new post in on the list
    And I click on create post button
    When I fill "{{lorem.words}}" for "title"
    And I fill "{{lorem.paragraphs}}" for "content"
    And I click "Save" button
    Then I see my new post on the list
    And my post has no likes
    # Like new post, ensure that can only like it once
    When I like my new post
    # Then my post has 1 like
    # And I cannot like it more than once
