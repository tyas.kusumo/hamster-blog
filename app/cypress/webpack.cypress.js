module.exports = {
  resolve: {
    extensions: ['.ts', '.js'],
  },
  node: { fs: 'empty', child_process: 'empty', readline: 'empty' },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: [/node_modules/],
        options: {
          transpileOnly: true,
        },
        loader: 'awesome-typescript-loader',
      },
      {
        test: /\.feature$/,
        loader: 'cypress-cucumber-preprocessor/loader',
      },
      {
        test: /\.features$/,
        loader: 'cypress-cucumber-preprocessor/lib/featuresLoader',
      },
    ],
  },
};
