import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given('I am logged in', () => {
  cy.visit('/');

  // TODO: this depends on fixture database
  cy.fixture('user.json').then(({ username, password }) => {
    cy.get('#username').type(username);
    cy.get('#password').type(password);
    cy.get('button:contains("Sign In")').click();
  });
});

When('I click on create post button', () => {
  cy.get('#create-post').click();
});

Then('I should see create post form', () => {
  cy.contains('Create Post');
  cy.screenshot('Create Post');
});

Then('I should not see create post form', () => {
  cy.get('body').should('not.contain', 'Create Post');
});

Then('I see my new post on the list', () => {
  cy.get('@title').then((postTitle) => {
    cy.scrollTo('top');
    cy.screenshot('Posts');
    cy.get('#posts-card-0').as('createdPost');
    cy.get('@createdPost')
      .children('#posts-title-0')
      .contains((postTitle as any) as string);
  });
});

Then('my post has no likes', () => {
  cy.scrollTo('top');
  cy.screenshot('No likes');
  cy.get('@createdPost')
    .first()
    .get('#posts-action-0')
    .contains('No likes yet');
});

When('I like my new post', () => {
  cy.get('@createdPost')
    .first()
    .get('#posts-action-0 > button[title="Like"]:first')
    .click();
});

Then('my post has no likes', () => {
  cy.scrollTo('top');
  cy.screenshot('1 like');
  cy.get('@createdPost').first().get('#posts-action-0').contains('1 Like');
});

Then('I cannot like it more than once', () => {
  cy.get('@createdPost')
    .first()
    .get('posts-action-0" > button[title="Like"]:first')
    .click();
  cy.get('@createdPost').first().get('#posts-action-0').contains('1 Like');
});
