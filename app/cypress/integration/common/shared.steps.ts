import { Then, When } from 'cypress-cucumber-preprocessor/steps';
import faker from 'faker';

Then(`I see {string} in the title`, (title) => {
  cy.get('h1:first').contains(title);
  cy.screenshot('Title');
});

When(`I fill {string} for {string}`, async (text, field) => {
  // Get from saved alias if starts with @
  if (text.startsWith('@')) {
    cy.get(text).then((alias) => {
      cy.get(`#${field}`).type((alias as any) as string);
    });
  } else {
    // Use faker by default
    const fakerText = faker.fake(text);
    cy.get(`#${field}`).type(fakerText);
    cy.wrap(fakerText).as(field);
  }
});

When(`I click {string} button`, (buttonTitle) => {
  cy.get(`button:contains("${buttonTitle}")`).click();
});

When(`I click {string} link`, (linkTitle) => {
  cy.get(`a:contains("${linkTitle}")`).click();
});

Then(`I see {string} message`, (alert) => {
  cy.contains(alert);
  cy.screenshot('Alert message');
});
