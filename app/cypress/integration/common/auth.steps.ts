import { Given, Then } from 'cypress-cucumber-preprocessor/steps';

Given('I am on the home page and I am not logged in', () => {
  cy.visit('/');
});

Given('I am on login page', () => {
  cy.visit('/');
});

Then('I see a login form', () => {
  cy.get('h1:first').contains('Sign in');
  cy.screenshot('Login form');
});

Then('I successfully logged out', () => {
  cy.get('h1:first').contains('Sign in');
  cy.screenshot('Login form');
});

Then('I see a registration form', () => {
  cy.get('h1:first').contains('Sign up');
  cy.screenshot('Registration form');
});

Then('I successfully logged in', () => {
  cy.get('h1#page-title').contains('Hamster Blog');
  cy.screenshot('Homepage');
});
