// see more example of adding custom commands to Cypress TS interface
// in https://github.com/cypress-io/add-cypress-custom-command-in-typescript
// add new command to the existing Cypress interface

// eslint-disable-next-line no-unused-vars
declare namespace Cypress {
  // eslint:disable-next-line interface-name
  interface Chainable {}
}
