
# Hamster Blogging Platform

Prototype of a blogging service for hamsters based on the following user stories:

- As a hamster, I want to sign up to a platform so I can connect with other the hamsters on the network
- As a social hamster, I want to be able to share posts with hamsters in the network
- As a social hamster, I want the hamsters in the network to like my posts

Feel free to use social single sign-on services such as Facebook and Google.

## Introduction

Deployed to:

 - [Frontend](http://dev-hamster-blog.s3-website-ap-southeast-2.amazonaws.com/)
 - [Backend](https://sr2ojh6j2e.execute-api.ap-southeast-2.amazonaws.com/dev/graphql)
   - Please modify the url in the playground to match the url. 

Automatic test results videos and screenshots are available on latest [pipeline](https://gitlab.com/tyas.kusumo/hamster-blog/pipelines)'s `post:deploy` artifacts.

Test credentials:
- username: `test`
- password: `test`

### Technical Stack

**Backend**

- [Nest](https://nestjs.com/)
- GraphQL
- Postgres
- Testing using [cucumber.js](https://github.com/cucumber/cucumber-js)

GraphQL [schema](./api/schema.graphql) automatically generated using [@nestjs/graphql](https://docs.nestjs.com/graphql/quick-start#code-first).

For more details see: [Backend README](./api/README.md)

**Frontend**

- React
- MobX State Tree
- Material UI
- Testing using storybook and [cypress](https://www.cypress.io/)

Base [models](./app/src/models/index.ts) for MobX State Tree automatically generated using [mst-gql](https://github.com/mobxjs/mst-gql) from the generated backend [schema](./api/schema.graphql).

For more details see: [Frontend README](./app/README.md)

## Getting Started

### Prerequisite

- [Yarn](https://yarnpkg.com/)
- [Node.js](https://nodejs.org/en/download/)
- [Docker](https://www.docker.com/)

Deployment requires [AWS](https://aws.amazon.com/) account.

### Installation

```bash
$ yarn install # Install packages
$ yarn init:env # Copy default environment variables
$ yarn build # Build the backend and frontend
$ yarn build:db # Create and load fixtures on local database
```

### Start

Start local server for backend and frontend:

```bash
$ yarn start
```

- Frontend endpoint: http://localhost:3000
- Backend endpoint: http://localhost:4000/graphql

Test credentials:
- username: `test`
- password: `test`

### Testing

Run integration tests:

```bash
$ yarn test:app:run # Runs in cli
$ yarn test:app:open # Opens the Cypress Test Runner
```

After running the test, screenshots and videos available at:
- `app/cypress/screenshots`
- `app/cypress/videos`

### Deployment

#### CI

Easiest way to deploy is to fork the project and run the pipeline after setting up the following CI variables:

- **ENV**:
  - The name of the `stage`
  - Example: `dev123`
- **DEPLOYMENT_REGION**:
  - AWS Region to deploy the service to
  - Example: `ap-southeast-2`
- **ENV_VARIABLES**
  - Credentials and Secret:
  - Example:
    ```bash
    DATABASE_NAME=graphql
    DATABASE_USERNAME=postgres
    DATABASE_PASSWORD=password
    JWT_SECRET=nothingToSeeHere
    ```
- **AWS_CREDENTIALS**: 
  - Your AWS [credentials](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html) for the associated **ENV**
  - Example:
    ```bash
    [dev123]
    aws_access_key_id = your_access_key_id
    aws_secret_access_key = your_secret_access_key
    ```
